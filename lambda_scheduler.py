# -*- coding: utf-8 -*-
"""
Created on Thu May 25 17:23:54 2017

@author: CCarrido
"""

import boto3
import datetime
import json
import re

CONFIG_TABLE_NAME = 'TagCompliance_Config'

# get a full list of the available regions
client = boto3.client('ec2')
regions_dict = client.describe_regions()
region_list = [region['RegionName'] for region in regions_dict['Regions']]
now = datetime.datetime.now()

# Use Security Token Service to get account ID
session = boto3.Session()
sts = session.client('sts')
accountId = sts.get_caller_identity()['Account']

def get_configuration(accountId):
    # Grab the item from DynamoDB
    ddb = session.resource('dynamodb')
    table = ddb.Table(CONFIG_TABLE_NAME)
    item = table.get_item(Key={'accountid': accountId})
    config = (item['Item']['config'])

    # Load default config items if they weren't specified
    config['evaluation_queue_name'] = config['evaluation_queue_name'] if 'evaluation_queue_name' in config else "TagCompliance_EvaluationQueue"

    return config


def addToScheduleQueue(accountId,region,resourceId,resourceType,config):
    print('The Account ID is: ' + accountId + '\n' \
          'The Region is: ' + region  + '\n' \
          'The Resource ID is: ' + resourceId  + '\n' \
          'The Resource Type is: ' + resourceType  + '\n')
    queueItem = {'accountId': accountId, \
                 'region': region, \
                 'resourceId': resourceId, \
                 'resourceType': resourceType}
    queueItem = json.dumps(queueItem)
    session = boto3.Session()
    sqs = session.resource('sqs')
    queue = sqs.get_queue_by_name(QueueName=config['evaluation_queue_name'])
    queue.send_message(MessageBody=queueItem)

    return

def get_account_list():
    ddb = session.resource('dynamodb')
    table = ddb.Table(CONFIG_TABLE_NAME)
    item = table.scan(AttributesToGet=['accountid'])
    tempitems = item.get("Items")
    results = []
    for items in tempitems:
        results.append(re.sub("[^0-9]", "", str(items)))

    return results

def lambda_handler(event, context):
    account_list = get_account_list()
    print("The accounts to check are: " + str(account_list))

    for account in account_list:
        config = get_configuration(account)
        print("Checking account number: " + account)

        for region in region_list:
            print('Checking ' + region + ' region...')
            # Check if cross-account role is needed
            if accountId != account:
                # Cross account access so we have to assume a role
                iam_role_name = config['scheduler_role_name']
                iam_role = "arn:aws:iam::{0}:role/{1}".format(account, iam_role_name)
                print("Assuming role {0} in account {1}".format(iam_role, account))
                sts_response = sts.assume_role(RoleArn=iam_role, RoleSessionName='lambda_session', DurationSeconds=900)
                credentials = sts_response['Credentials']
                ec2 = boto3.resource('ec2',
                          region_name=region,
                          aws_access_key_id = credentials['AccessKeyId'],
                          aws_secret_access_key = credentials['SecretAccessKey'],
                          aws_session_token = credentials['SessionToken'])
            else:
                ec2 = boto3.resource('ec2', region_name=region)
            for instance in ec2.instances.all():
                if instance.tags == None: #handles case of no tags at all
                    print(instance.instance_id + ' Scheduled for audit due to missing tagCompliance_LastAudit tag.')
                    addToScheduleQueue(account,region,instance.instance_id,"AWS::EC2::Instance",config)
                else:
                    if 'tagCompliance_LastAudit' not in [tag['Key'] for tag in instance.tags]:
                        print(instance.instance_id + ' Scheduled for audit due to missing tagCompliance_LastAudit tag.')
                        addToScheduleQueue(account,region,instance.instance_id,"AWS::EC2::Instance",config)
                    for tag in instance.tags:
                        if tag['Key'] == 'tagCompliance_LastAudit':
                            print('The tagCompliance_LastAudit Value is: ' + tag['Value'])
                            lastAuditDate = datetime.datetime.strptime((tag['Value']),'%Y-%m-%d %H:%M:%S')
                            evaluatedDate = lastAuditDate + datetime.timedelta(days=1)
                            # if date is more than configured time, then add to SQS Evaluate_Compliance
                            if now > lastAuditDate + datetime.timedelta(days=1):
                                print(instance.instance_id + ' Scheduled for audit due to time since last audit exceeded.')
                                newDate = lastAuditDate + datetime.timedelta(days=1)
                                print('Date evaluated was: ' + str(evaluatedDate))
                                addToScheduleQueue(account,region,instance.instance_id,"AWS::EC2::Instance",config)
                            else:
                                print(instance.instance_id + ' Not scheduled for audit since it was recently audited.')
                                print('Date evaluated was: ' + str(evaluatedDate))