import boto3
import json

CONFIG_TABLE_NAME = 'TagCompliance_Config'

session = boto3.Session()
sts = session.client('sts')
accountid = sts.get_caller_identity()['Account']

# Grabs the configuration items from DynamoDB and loads them into a dict
# RETURNS: (dict) config
def get_configuration(accountId):
    # Grab the item from DynamoDB
    ddb = session.resource('dynamodb')
    table = ddb.Table(CONFIG_TABLE_NAME)
    item = table.get_item(Key={'accountid': accountId})
#    print("the account configuration for this item is: " + str(item))
    config = (item['Item']['config'])

    # Load default config items if they weren't specified
    config['execution_queue_name'] = config['execution_queue_name'] if 'execution_queue_name' in config else "TagCompliance_ExecutionQueue"

    try:
        config['max_batch_size'] = int(config['max_batch_size']) if 'max_batch_size' in config else 10

        if config['max_batch_size'] > 10:
            config['max_batch_size'] = 10
    except ValueError:
        # It probably wasn't a number so let's go with a safe default
        config['max_batch_size'] = 10

    return config

def perform_action(payload):
    resource_id = payload['resourceId']
    region = payload['region']
    resourceType = payload['resourceType']
    payload_accountid = payload['accountId']
    action = payload['action']

    print("Payload account ID is " + payload_accountid)
    print("Payload action is " + action)
    configuration = get_configuration(payload_accountid)
    #print("Perform Action function, configuration is " + str(configuration))
    iam_role_name = configuration['executor_role_name']
    if "EC2::Instance" in resourceType:
        if accountid != payload_accountid:
            # Cross account access so we have to assume a role
            iam_role_name = configuration['executor_role_name']
            iam_role = "arn:aws:iam::{0}:role/{1}".format(payload_accountid, iam_role_name)
            print("Assuming role {0} in account {1}".format(iam_role, payload_accountid))
            sts_response = sts.assume_role(RoleArn=iam_role, RoleSessionName='lambda_session', DurationSeconds=900)
            credentials = sts_response['Credentials']
            ec2 = boto3.resource('ec2',
                      region_name=region,
                      aws_access_key_id = credentials['AccessKeyId'],
                      aws_secret_access_key = credentials['SecretAccessKey'],
                      aws_session_token = credentials['SessionToken'])

        else:
        # Load the instance data
#           ec2 = session.resource('ec2', region_name=region)
            ec2 = session.resource('ec2')
        instance = ec2.Instance(resource_id)

        try:
            if action == "stop":
                print("Stopping instance with ID: {0}".format(resource_id))
                instance.stop()
            elif action == "terminate":
                print("Terminating instance with ID: {0}".format(resource_id))
                instance.terminate()
            else:
                print("No proper action specified. Action: {0}".format(action))
        except Exception as e:
            print("Failed to perform action: {0}".format(e))


def lambda_handler(event, context):
    print("Starting tagging compliance executor...")

    core_configuration = get_configuration(accountid)
    sqs = session.resource('sqs')

    # Get the SQS Queue and messages
    queue = sqs.get_queue_by_name(QueueName=core_configuration['execution_queue_name'])
    messages = queue.receive_messages(MaxNumberOfMessages=core_configuration['max_batch_size'])


    if len(messages) > 0:
        print("Processing {0} messages...".format(len(messages)))
        # Process the messages
        for message in messages:
            if message.body != "":
                try:
                    # Convert the body string to json
                    messageJSON = json.loads(message.body)
                except ValueError:
                    print("The message did not contain valid JSON! Raw string: {0}".format(message.body))
                    continue

                perform_action(messageJSON)

            message.delete()

        print("Processing of all messages complete. Exiting.")

    else:
        print("No messages to process this run. Exiting.")