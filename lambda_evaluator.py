import sys
import json
import boto3
from datetime import datetime
from datetime import timedelta

CONFIG_TABLE_NAME = 'TagCompliance_Config'

session = boto3.Session()

# Grabs the configuration items from DynamoDB and loads them into a dict
# RETURNS: (dict) config
def get_configuration(accountId):
    # Grab the item from DynamoDB
    ddb = session.resource('dynamodb')
    table = ddb.Table(CONFIG_TABLE_NAME)
    item = table.get_item(Key={'accountid': accountId})
#    print("the account configuration for this item is: " + str(item))
    config = (item['Item']['config'])

    # Load default config items if they weren't specified
    config['action_notify'] = config['action_notify'] if 'action_notify' in config else True
    config['action_stop'] = config['action_stop'] if 'action_stop' in config else True
    config['action_terminate'] = config['action_terminate'] if 'action_terminate' in config else False
    config['warn_before_action'] = config['warn_before_action'] if 'warn_before_action' in config else True
    config['notification_recipients'] = config['notification_recipients'] if 'notification_recipients' in config else ["gco@beckman.com"]
    config['evaluation_queue_name'] = config['evaluation_queue_name'] if 'evaluation_queue_name' in config else "TagCompliance_EvaluationQueue"
    config['execution_queue_name'] = config['execution_queue_name'] if 'execution_queue_name' in config else "TagCompliance_ExecutionQueue"
    config['minimum_tags'] = item['Item']['minimum_tags']

    # Try to convert that max batch size to an int for later
    try:
        config['max_batch_size'] = int(config['max_batch_size']) if 'max_batch_size' in config else 10

        if config['max_batch_size'] > 10:
            config['max_batch_size'] = 10
    except ValueError:
        # It probably wasn't a number so let's go with a safe default
        config['max_batch_size'] = 10

    return config

def send_notification(violation_text, recipients, message_details):
    print("Sending notification to {0}".format(recipients))

    if recipients:
        ses = session.client('ses')

        subject = "AWS Instance Tagging Violation - Account: {0} - Region: {1}".format(message_details['accountid'], message_details['region'])
        message_body = ("An AWS resource is currently violating the tagging policy.\n\n"
                        "AWS Account ID: {0}\n"
                        "Region: {1}\n"
                        "ID: {2}\n\n"
                        "Violation Details:{3}\n\n"
                        "Remediation: {4}").format(message_details['accountid'],
                                                message_details['region'],
                                                message_details['instanceid'],
                                                violation_text,
                                                message_details['action_result']
                                                )

        try:
            ses.send_email(Source='no-reply@beccloud.com',
                            Destination={'ToAddresses': recipients},
                            Message={
                                'Subject': {
                                    'Data': subject,
                                    'Charset': 'UTF-8'
                                },
                                'Body': {
                                    'Text': {
                                        'Charset': 'UTF-8',
                                        'Data': message_body
                                    }
                                }
                            }
                        )
        except Exception as e:
            print("Failed to send message: {0}".format(e))

# Checks a dict of current_tags against a dict of required_tags.
# RETURNS: a string, violation, with details of the problem(s). The string is empty if there is no issue.
def find_violation(current_tags, required_tags):

    if not current_tags:
        violation = "Instance has no tags!"
    else:
        violation = ""
        for rtag,rvalues in required_tags.items():
            tag_present = False
            for tag in current_tags:
                if tag['Key'] == rtag:
                    value_match = False
                    tag_present = True
                    rvaluesplit = rvalues.split(",")
                    for rvalue in rvaluesplit:
                        if tag['Value'] == rvalue:
                            value_match = True
                        if tag['Value'] != "":
                            if rvalue == "*":
                                value_match = True
                    if value_match == False:
                        violation = violation + "\n" + tag['Value'] + " doesn't match any of " + required_tags[rtag] + "!"
            if not tag_present:
                violation = violation + "\n" + "Tag " + str(rtag) + " is not present."

    if violation == "":
        return None
    else:
        return  violation

def check_last_audit_tag(resource):
    do_audit = True

    if resource.tags:
        if next((item for item in resource.tags if item['Key'].upper() == 'TAGCOMPLIANCE_LASTAUDIT'), None):
            # the instance contains the LastAudit tag so let's analyze it
            lastaudit = datetime.strptime(next((item for item in resource.tags if item['Key'].upper() == 'TAGCOMPLIANCE_LASTAUDIT'))['Value'], '%Y-%m-%d %H:%M:%S')

            if datetime.today() - timedelta(hours=24) < lastaudit:
                do_audit = False
                print("Instance with id {0} has been audited recently. Nothing to do.".format(resource.id))

    return do_audit

def update_last_audit_tag(resource):
    #if resource.tags:
        #if next((item for item in resource.tags if item['Key'].upper() == 'TAGCOMPLIANCE_LASTAUDIT'), None):
    resource.create_tags(Tags=[{'Key': 'tagCompliance_LastAudit', 'Value': datetime.now().strftime('%Y-%m-%d %H:%M:%S')}])

def schedule_action(config, resource, resourceType, accountid, region):
    perform_action = True
    action_result = ""
    deadline_passed = False

    if resource.tags:
        if config['warn_before_action']:
            if next((item for item in resource.tags if item['Key'].upper() == 'TAGCOMPLIANCE_DEADLINE'), None):
                deadline = datetime.strptime(next((item for item in resource.tags if item['Key'].upper() == 'TAGCOMPLIANCE_DEADLINE'))['Value'], '%Y-%m-%d %H:%M:%S')
                print("The deadline is {0} for instance with id {1}".format(deadline, resource.id))

                if deadline > datetime.today():
                    perform_action = False
                else:
                    deadline_passed = True
            else:
                # Add a deadline
                deadline = (datetime.today() + timedelta(hours=4)).strftime("%Y-%m-%d %H:%M:%S")
                resource.create_tags(Tags=[{'Key': 'TagCompliance_Deadline', 'Value': deadline}])
                action_result = "Deadline of {0} given for remediation. Please resolve the issue to avoid the instance being stopped or terminated.".format(deadline)
                perform_action = False

    if perform_action:
        if config['action_terminate']:
            action = "terminate"
        elif config['action_stop']:
            action = 'stop'
        else:
            action = 'notify'

        payload = {}
        payload['resourceId'] = resource.id
        payload['region'] = region
        payload['resourceType'] = resourceType
        payload['accountId'] = accountid
        payload['action'] = action

        # Convert dict to JSON
        myjson = json.dumps(payload)

        print("Scheduling {0} for instance with id {1}".format(action, resource.id))

        # Create a message in SQS for the action lambda to pickup later
        sqs = session.resource('sqs')
        queue = sqs.get_queue_by_name(QueueName=config['execution_queue_name'])
        queue.send_message(MessageBody=myjson)

        if deadline_passed:
            resource.delete_tags(Tags=[{'Key': 'TagCompliance_Deadline'}])

        action_result = "The listed action will be taken against the instance shortly. Action: {0}".format(action)
    else:
        print("Instance with id {0} has a deadline in the future. Nothing to do for it right now.".format(resource.id))

    return action_result

def lambda_handler(event, context):
    print("Starting tagging compliance evaluator...")

    sts = session.client('sts')
    accountid = sts.get_caller_identity()['Account']

    core_configuration = get_configuration(accountid)
    sqs = session.resource('sqs')

    # Get the SQS Queue and messages
    queue = sqs.get_queue_by_name(QueueName=core_configuration['evaluation_queue_name'])
    messages = queue.receive_messages(MaxNumberOfMessages=core_configuration['max_batch_size'])

    if len(messages) > 0:
        print("Processing {0} messages...".format(len(messages)))
        # Process the messages
        for message in messages:
            if message.body != "":
                try:
                    # Convert the body string to json
                    messageJSON = json.loads(message.body)
                except ValueError:
                    print("The message did not contain valid JSON! Raw string: {0}".format(message.body))
                    continue

                print("Account ID is {0}".format(messageJSON['accountId']))
                print("Resource type is {0}".format(messageJSON['resourceType']))
                configuration = get_configuration(messageJSON['accountId'])

                # TODO: Add EC::Volume later
                # Determine the resource Type
                if "EC2::Instance" in messageJSON['resourceType']:
                    print("Processing EC2 instance with id {0}".format(messageJSON['resourceId']))

                    if messageJSON['accountId'] != accountid:
                        # Cross account access so we have to assume a role
                        iam_role_name = configuration['evaluator_role_name']
                        iam_role = "arn:aws:iam::{0}:role/{1}".format(messageJSON['accountId'], iam_role_name)
                        print("Assuming role {0} in account {1}".format(iam_role, messageJSON['accountId']))
                        sts_response = sts.assume_role(RoleArn=iam_role, RoleSessionName='lambda_session', DurationSeconds=900)
                        credentials = sts_response['Credentials']
                        ec2 = boto3.resource('ec2',
                                  region_name=messageJSON['region'],
                                  aws_access_key_id = credentials['AccessKeyId'],
                                  aws_secret_access_key = credentials['SecretAccessKey'],
                                  aws_session_token = credentials['SessionToken'])

                    else:
                    # Load the instance data
                        ec2 = session.resource('ec2', region_name=messageJSON['region'])
                    instance = ec2.Instance(messageJSON['resourceId'])
                    instance.load()
                    do_audit = check_last_audit_tag(instance)
                    if do_audit:
                        # Send it off for evaluation
                        violation_text = find_violation(instance.tags, configuration['minimum_tags'])

                        update_last_audit_tag(instance)

                        action_result = ""

                        if violation_text:
                            print("Instance has violations: {0}".format(violation_text))
                            action_result = schedule_action(configuration, instance, messageJSON['resourceType'], messageJSON['accountId'], messageJSON['region'])

                        if configuration['action_notify'] and action_result:
                            recipients = list(configuration['notification_recipients'])

                            if instance.tags:
                                add_recipient = next((item for item in instance.tags if item['Key'].upper() == 'CREATEDBY'), None)

                                if add_recipient:
                                    recipients.append(add_recipient['Value'])

                            message_details = {'accountid': messageJSON['accountId'], 'region': messageJSON['region'], 'instanceid': instance.instance_id, 'action_result': action_result}

                            send_notification(violation_text, recipients, message_details)

                elif "RDS" in messageJSON['resourceType']:
                    print("It's an RDS instance")
                else:
                    print("Evaluation of resource type: {0} is not supported.".format(messageJSON['resourceType']))

            message.delete()
    else:
        print("No messages to process this run. Exiting.")